Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/linux-msm/qbootctl
Upstream-Name: qbootctl
Upstream-Contact: Caleb Connolly <caleb@connolly.tech>

Files: *
Copyright: 2021-2023 Caleb Connolly <caleb@connolly.tech>
License: GPL-3+

Files: bootctrl.h
Copyright:
 2016 The Android Open Source Project
 2021-2023 Caleb Connolly <caleb@connolly.tech>
License: GPL-3+

Files: bootctrl_impl.c
Copyright:
 2016 The Linux Foundation
 2021-2023 Caleb Connolly <caleb@connolly.tech>
License: GPL-3+

Files: crc32.c
Copyright: 1986 Gary S. Brown
           2000 Matt Domsch <Matt_Domsch@dell.com>
License: GPL-2

Files: crc32.h
Copyright: 1998-2000 Free Software Foundation, Inc
License: GPL-2

Files: gpt-utils.c
Copyright: 2013 The Linux Foundation
           2021-2022 Caleb Connolly <caleb@connolly.tech>
License: BSD-3-Clause

Files: gpt-utils.h
       ufs-bsg.*
Copyright: 2013-2020 The Linux Foundation
License: BSD-3-clause

Files: debian/*
Copyright: 2023 Dmitry Baryshkov <dbaryshkov@gmail.com>
           2025 Arnaud Ferraris <aferraris@debian.org>
License: GPL-3+

Files: debian/qbootctl.service
Copyright: 2018 Lennart Poettering <lennart@poettering.net>
License: LGPL-2.1+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
     * Neither the name of The Linux Foundation nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".
